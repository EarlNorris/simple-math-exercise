# Generating tables for arithmetic exercises: addition, subtraction, multiplication, and division

for i in range(1, 10):
    for j in range(1, 10):
        addition = f"{i} + {j} = {i+j}"
        subtraction = f"{i} - {j} = {i-j}"
        multiplication = f"{i} x {j} = {i*j}"
        division = f"{i} ÷ {j} = {round(i/j, 2)}"
        print(f"{addition}\t{subtraction}\t{multiplication}\t{division}")
        
